# vrpn_client_ros
This for aims at porting the original code from the kinetic-devel branch to ROS2.

## Requirements

The code requires VRPN to work. A way can be to do : 

```
    apt-get install ros-melodic-vrpn
```

Or to transfer all the vprn files from the ROS1 to the ROS2 install (so all the files in include, lib, bin, ... with vrpn on them). You also need to take other libraries (ex : libquat), but the errors at compilation are enoughly precise to deal with them.

## What works?

I just took the work from https://github.com/zeroos/vrpn_client_ros/tree/foxy-devel and port the TF. So, TF and Pose are working, but not twist or acceleration.
